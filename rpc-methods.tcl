#! eggdrop
#
# Services exposed by eggdrop-rpc. This is mostly an example that just plain
# wraps around some of eggdrop's commands.
#
# You must sanitize your own inputs as needed, they're guaranteed not to
# contain NUL bytes, but that's it. The parameters are always going to contain
# a list of unsanitized strings.
#
# Defining procedures with the "rpc" command calculates the minimum and maximum
# number of arguments for you. If for some reason the minimum number of
# arguments is shorter than the argument list, you can override it by passing
# it before the argument list. See "getuser" for an example.
#
# Finally, you can reload this script as needed if you added, changed or
# removed methods.
#

namespace eval ::rpc {

variable methods {}

# A simple echo server
rpc echo {args} {
	return $args
}

# authentication server
rpc checkpass {user pass} {
	passwdok $user $pass
}

# get user attributes. Takes a minimum of 2 and a maximum of 3 parameters
rpc getuser 2 {user entry extra} {
	if {$extra ne ""} {
		getuser $user $entry $extra
	} else {
		getuser $user $entry
	}
}

# change a user's password
rpc changepass {user newpass} {
	setuser $user PASS [encpass $newpass]
}

# list all methods this server supports
rpc methods {} {
	variable methods
	return $methods
}

# execute an arbitrary Tcl command and return its value.
# enable this only if you need to, it is better to write proper RPC methods for
# things you want to support in your application. I use this for debugging.
#rpc tcl {script} {
#	eval $script
#}

# creates a user with no hosts and no password.
rpc adduser {user} {
	return [adduser $user]
}

};# end namespace
