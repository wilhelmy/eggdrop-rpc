#! eggdrop
#
# This is an RPC service wrapped in an interp.
#

package require Tcl 8.5

namespace eval ::rpc {

array set settings {
	# { How many simultaneous connections are allowed in total? }
	max-connections		5

	# { Which IPs are allowed to connect? This is currently limited to
	#   localhost for security reasons. If your web server or application
	#   runs on a different machine than your eggdrop, you will need to add
	#   all IPs that need to connect to the RPC interface to this list.
	#   Values are separated by a space character. }
	allowed-hosts 		{127.0.0.1 ::1}

	# { What port to listen on? Must be a number between 1024 and 65535 }
	listen-port		1337

	# { What address to listen on? By default, the module will only bind to
	#   the local address. Set to "*" to listen on all interfaces, or to a
	#   different IP to just listen on that IP.
	#
	### WARNING! #####
	#   IF YOU CHANGE THIS, MAKE SURE AUTHENTICATION AND TLS ARE ENABLED! }
	listen-addr		127.0.0.1

	# { These options are only relevant if you enable TLS. If you allow
	#   connections from a different host, you should make sure you enable
	#   TLS! See README.TLS for more information }
	use-tls			no
	tls-cert		"tcl/rpc/rpc.cert"
	tls-key 		"tcl/rpc/rpc.key"
	tls-options             {}
	tls-ciphers		"HIGH:!aNULL:!eNULL:!EXPORT:!LOW:!DES:!RC4:!MD5:!PSK"

	# { authentication-methods can be either none, plaintext-password or a
	#   SASL mechanism you want to use. If you want to use SASL, you need
	#   to install the SASL package, which is part of tcllib.
	#
	#   You probably don't want to set this to none, because it is
	#   insecure and only useful for debugging this module by hand.
	#
	#   Make sure you also change the password }
	authentication-method	plaintext-password

	# { change this }
	password		"Satan Calculus is coming to town"

	# { specifies the loglevel for connect/disconnect events, see
	#   eggdrop.conf (search for "LOG FILES") for a list. In the log
	#   string, %s is replaced by IP and port of the peer..
	#
	#   By default, eggdrop-rpc logs everything to one of eggdrop's log
	#   levels that make sense for the particular purpose, as well as log
	#   level 7 which is intended for logging RPC related things to a
	#   separate log file. For this to work, see eggdrop.conf about logging
	#   options }
	loglevel-connect	d7
	logstring-connect	"RPC: %s"
	
	# { loglevel for clients which were kicked for misbehaving }
	loglevel-jackass	o7
	logstring-jackass	"RPC: %s kicked:"

	# { loglevel for internal programming errors }
	loglevel-bugs		o7
	logstring-bugs		"RPC: %s triggered bug (please report):"

	# { loglevel for general stuff }
	loglevel-info		o7
	logstring-info		"RPC: %s"

	# #
}

variable ns [namespace current]
variable VERSION 0.0+git
array set idle {}
array set sasl {}
array set peers {}
array set buffers {}
array set authenticated {}

proc accept {chan addr port} {
	variable ns
	variable idle
	variable sasl
	variable peers
	variable buffers
	variable settings
	variable authenticated

	fconfigure $chan -blocking 0 -encoding binary -translation binary

	set peers($chan) [list $addr $port]
	# Doesn't hurt to be a bit paranoid
	if {$addr ni $settings(allowed-hosts)} {
		log_jackass "connection attempt which is not allowed"
		unset peers($chan)
		close $chan
		return
	}
	
	if {[array size idle] >= $settings(max-connections)} {
		rpc_error $chan "Too many connections, try again later"
		log_jackass "max-connections exceeded"
		unset peers($chan)
		close $chan
		return
	}

	set cl [expr [array size idle]+1]
	log_connect "connected, $cl currently connected"

	if {$settings(use-tls)} {
		fileevent $chan readable [list ${ns}::tls_handshake $chan ${ns}::recv]
	} else {
		fileevent $chan readable [list ${ns}::recv $chan]
	}

	set sasl($chan) {}
	set idle($chan) [clock seconds]
	set buffers($chan) {}
	set authenticated($chan) 0

	auth_begin $chan
}

# "prettyprint" an rpc message by replacing all \0 by :
proc rpc_pp {msg} {
	return [string map {\0 :} $msg]
}

proc rpc_split {chan} {
	variable buffers

	set offset [string first "\0\0\0\0" $buffers($chan)]

	if {$offset == -1} {
		return {}
	}

	set res [split [string range $buffers($chan) 0 $offset-1] \0]
	set buffers($chan) [string range $buffers($chan) $offset+4 end]
	return $res
}

proc rpc_send {chan args} {
	puts -nonewline $chan "[join $args \0]\0\0\0\0"
	flush $chan
}

proc b64dec {chan arg} {
	# wrapper around base64::decode that verifies that anything can be
	# base64 at all.
	set r {^(?:[A-Za-z0-9+/]{4})*(?:[A-Za-z0-9+/]{2}==|[A-Za-z0-9+/]{3}=)?$}
	if {![regexp $r $arg] || [catch {base64::decode $arg} b64]} {
		rpc_error $chan "internal error"
		log_jackass "invalid base64"
		disconnect $chan
		return -code return
	}
	return $b64
}

proc b64enc {arg} {
	if {$arg ne ""} {
		base64::encode -maxlen 0 $arg
	}
}

# for the sake of clarity and avoiding mistakes by redefinition, define the
# protocol internals
proc rpc_error {chan errmsg} {
	rpc_send $chan ERROR $errmsg
}

proc rpc_ok {chan args} {
	rpc_send $chan OK {*}$args
}

proc rpc_auth_begin {chan {par {}}} {
	variable settings

	set par [b64enc $par]
	rpc_send $chan \
		AUTHENTICATION BEGIN $settings(authentication-method) {*}$par
}

proc rpc_auth_continue {chan {par {}}} {
	set par [b64enc $par]
	rpc_send $chan AUTHENTICATION CONTINUE {*}$par
}

# disconnects a client and removes all data that might be left over
proc disconnect {chan} {
	variable idle
	variable sasl
	variable peers
	variable buffers
	variable authenticated
	catch {unset idle($chan)}
	catch {unset sasl($chan)}
	catch {unset peers($chan)}
	catch {unset buffers($chan)}
	catch {unset authenticated($chan)}
	catch {close $chan}
}

proc saslauth_callback {ctx command args} {
	variable settings
	switch -exact -- $command {
		login    { return "" }
		username { return dieter }
		password { return $settings(password) }
		realm    { return "" }
		hostname { return [info host] }
		default  { return -code error unexpected }
	}
}

proc auth_begin {chan} {
	variable ns
	variable sasl
	variable settings
	variable authenticated

	switch -exact -- $settings(authentication-method) {
		"none" {
			set authenticated($chan) 1
			rpc_ok $chan
			return
		}

		"plaintext-password" {
			set par {}
		}

		default {
			set sasl($chan) [SASL::new -type server                \
				-service "eggdrop-rpc"                         \
				-server  $settings(listen-addr)                \
				-mechanism $settings(authentication-method)    \
				-callback ${ns}::saslauth_callback]
			SASL::step $sasl($chan) ""
			set par [SASL::response $sasl($chan)]
		}
	}
	rpc_auth_begin $chan $par
}

# pretty-print a peer
proc pp_p {chan} {
	variable peers
	lassign $peers($chan) ip port
	return "$ip/$port"
}

# This isn't an interactive protocol, just kick clients that fail authenticatiuon 
proc auth_fail {chan {msg ""}} {
	log_jackass "authentication failed $msg"
	rpc_error $chan "authentication failed"
	disconnect $chan
	return -code return
}

proc auth_step {chan arg} {
	variable sasl
	variable settings
	variable authenticated

	lassign $arg authenticate auth

	if {$authenticate ne "authenticate"} {
		auth_fail $chan "client sent method call during authentication"
	}

	# internal authentication
	if {$settings(authentication-method) eq "plaintext-password"} {
		if {$settings(password) eq $auth} {
			set authenticated($chan) 1
			rpc_ok $chan
			return
		} else {
			auth_fail $chan
		}
	}

	# SASL
	set auth [b64dec $chan $auth]
	if {[catch {SASL::step $sasl($chan) $auth} step]} {
		if {$step eq "authentication failed"} {
			auth_fail $chan
		} else {
			auth_fail $chan "SASL error: $::errorInfo"
		}
	} elseif {$step == 0} { # success
		SASL::cleanup $sasl($chan)
		set authenticated($chan) 1
		rpc_ok $chan
	} else {
		rpc_auth_continue $chan [SASL::response $sasl($chan)]
	}
}

proc recv {chan} {
	variable idle
	variable buffers

	if {[catch {read $chan} data] || [eof $chan]} {
		set cl [expr [array size idle]-1]
		log_connect "disconnected, $cl currently connected"
		disconnect $chan
		return
	}

	append buffers($chan) $data

	while {[set arg [rpc_split $chan]] != {}} {
		rpc_exec $chan $arg
	}
}

# execute an rpc command as if it were coming from the specified channel
proc rpc_exec {chan arg} {
	variable idle
	variable methods
	variable settings
	variable authenticated

	# ignore nops completely
	if {[lindex $arg 0] eq "nop"} {
		return
	}

	if {$authenticated($chan) < 1} {
		auth_step $chan $arg
		return
	}

	set idle($chan) [clock seconds]

	if {[lindex $arg 0] ni $methods} {
		rpc_error $chan "invalid method"
		log_info "tried invalid method [lindex $arg 0]"
		return
	}
	if {[catch {rpc_ok $chan {*}[sandbox eval $arg]} err]} {
		rpc_error $chan "internal error"
		log_bug $err
	}
}

# script reloaded?
if {[info exists ${ns}::server]} {
	close $server
	unset server
}

## initalize sandbox
catch {rename sandbox {}} ;# kill the previous sandbox, if any
interp create -safe ${ns}::sandbox

# hide all default commands
foreach cmd [sandbox eval info commands] {
	sandbox hide $cmd
}
# TODO time limits

# from tls/tests/simpleServer.tcl
proc tls_handshake {chan cmd} {
	if {[eof $chan]} {
		log_jackass "client disconnected during TLS handshake"
		disconnect $chan
	} elseif {[catch {tls::handshake $chan} result]} {
		# Some errors are normal. Specifically, I (hobbs) believe that
		# TLS throws EAGAINs when it may not need to (or is inappropriate).
		log_jackass "TLS handshake error: $result"
		disconnect $chan
	} elseif {$result == 1} {
		fileevent $chan readable [list $cmd $chan]
	}
}

proc create_server {} {
	variable ns
	variable settings
	variable VERSION

	set cmd {}

	if {$settings(use-tls)} {
		lappend cmd tls::socket -ssl2 0 -ssl3 0 -tls1 1 \
			-cipher   $settings(tls-ciphers)        \
			-keyfile  $settings(tls-key)            \
			-certfile $settings(tls-cert)           \
			{*}$settings(tls-options)
	} else {
		lappend cmd socket
	}
	lappend cmd -server ${ns}::accept

	if {$settings(listen-addr) ne "*"} {
		lappend cmd -myaddr $settings(listen-addr)
	}

	lappend cmd $settings(listen-port)

	set tls [expr {$settings(use-tls) ? " (TLS)" : ""}]
	putloglev $settings(loglevel-info) * \
	"Eggdrop-RPC $VERSION listening on $settings(listen-addr):$settings(listen-port)$tls"

	eval $cmd
}

proc _log {level args} {
	upvar chan chan; #hack
	putloglev $level * [format [join $args] [pp_p $chan]]
}

interp alias {} log_connect {} ${ns}::_log $settings(loglevel-connect) $settings(logstring-connect)
interp alias {} log_jackass {} ${ns}::_log $settings(loglevel-jackass) $settings(logstring-jackass)
interp alias {} log_bug     {} ${ns}::_log $settings(loglevel-bugs)    $settings(logstring-bugs)
interp alias {} log_info    {} ${ns}::_log $settings(loglevel-info)    $settings(logstring-info)

# helper for rpc below, checks the number of parameters for an RPC routine.
proc rpcproc {name min max argl} {
	upvar 1 args _args
	set nargs [llength $_args]

	if {$nargs < $min} {
		return -code error "$name: invalid # args, need at least $min"
	} elseif {$max != -1 && $nargs > $max} {
		return -code error "$name: invalid # args, need at most $max"
	} elseif {$argl != {}} {
		# lassign doesn't work with zero parameters
		set _args [uplevel 1 [list lassign $_args {*}$argl]]
	}
}

# define an RPC method
proc rpc {args} {
	variable ns
	variable methods

	set min 0
	if {[llength $args] == 4} {
		lassign $args name min argl script
	} else {
		lassign $args name argl script
	}

	set variadic [expr {[lindex $argl end] eq "args"}]
	set max [llength $argl]

	if {$variadic} {
		set argl [lreplace $argl end end] ;# get rid of "args"
		incr nargl -1
		set max -1
	}

	if {$min == 0} {
		set min [llength $argl]
	}

	set script [concat [list rpcproc $name $min $max $argl] \; $script]
	uplevel 1 [list proc RPC_$name {args} $script]

	lappend methods $name
	interp alias ${ns}::sandbox $name {} ${ns}::RPC_$name
}

if {$settings(use-tls)} {
	# older versions are untested but might work.
	package require tls 1.6
}

if {$settings(authentication-method) ni {none plaintext-password}} {
	# older versions are untested but might work.
	package require SASL 1.3
	package require base64 2.3
}

variable server [create_server]

} ;# end namespace
