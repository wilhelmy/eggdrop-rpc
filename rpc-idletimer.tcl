#! eggdrop
#
# This is an RPC service wrapped in an interp. - This file takes care of
# disconnecting idle clients. You might not want that, so it's up to you
# whether to load it or not.
#

namespace eval ::rpc {

array set settings {
	# { How often do you want to run the code that purges lingering
	#   connections? Default is every 5 minutes }
	cron-interval		"*/5 * * * *"

	# { How many seconds after the last activity do we consider connections
	#   to have become stale? Default is 5 minutes, which might be a bit
	#   too high. }
	stale-after-seconds	300
	# #
}

variable ns [namespace current]
array set idle {}

# run from "bind cron" to kill off sockets older than
# $settings(stale-after-seconds) seconds
proc cleanup {args} {
	variable idle
	variable settings

	set now [clock seconds]

	foreach {sock idle_since} [array get idle] {
		if {$now - $idle_since > $settings(stale-after-seconds)} {
			disconnect $sock
		}
	}
}

bind cron - $settings(cron-interval) ${ns}::cleanup

} ;# end namespace
