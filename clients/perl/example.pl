#!/usr/bin/env perl
#

BEGIN { unshift @INC, "."; }

use Module::Load;
load './EggdropRPC.pm', qw/rpc_split rpc_format rpc_complete/;

print join(':', rpc_split("echo\0hello\0world\0\0\0\0")), "\n";

# v incomplete data, last \0 missing
print rpc_split("echo\0hello\0world\0\0\0"), "\n";

my @args = ('echo','hello','world');
my $rpc = rpc_format(@args);
# more pleasant to verify if there's no embedded nul bytes on your terminal, ymmv.
$rpc =~ s/\0/:/g;
print $rpc,"\n";
