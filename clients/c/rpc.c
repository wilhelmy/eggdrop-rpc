#include <stdlib.h>
#include <string.h>

int rpc_complete(char const *data, size_t len)
{
	char const *p = data + len - 1;             /* last character of data */
	return  *(p--) == '\0' &&
	        *(p--) == '\0' &&
	        *(p--) == '\0' &&
	        *(p--) == '\0';
}

char const **rpc_split(char const *data, size_t len, size_t *rlen)
{
	char const **result, **pos;
	size_t nmemb, i;
	
	                               /* is the message terminated properly? */
	if (!rpc_complete(data, len))
		return NULL;

	                             /* how big is the output is going to be? */
	for (i = 0, nmemb = 1; i < len - 5; i++)
		if (data[i] == '\0')
		       nmemb++;
 
	if (!(result = malloc(sizeof(char const *) * (nmemb + 1))))
		return NULL;   /* errno = ENOMEM now, so this is distinguishable
		                * from a message missing the trailer */

	pos = result;
	*(pos++) = data;

	for (i = 0; i < len - 5; i++)
		if (data[i] == '\0')                         /* next element? */
			*(pos++) = data + i + 1;       /* add it to the list. */

	*(pos++) = NULL;

	if (rlen)
		*rlen = nmemb;

	return result;
}

char *rpc_format(char const **rpc, size_t *size)
{
	size_t i, total, len;
	char *pos, *result;

	for (i = 0, total = 0; rpc[i]; i++)     /* how much space do we need? */
		total += strlen(rpc[i]);

	total += i + 4;                        /* reserve space for NUL bytes */

	if (!(result = malloc(total)))
		return NULL;

	/* make sure that the trailer is always complete, even if the loop is
	 * never run because there are zero elements in *rpc */
	pos = result + total - 1;
	*(pos--) = '\0';
	*(pos--) = '\0';
	*(pos--) = '\0';
	*(pos--) = '\0';

	for (i = 0, pos = result; rpc[i]; i++) {
		len = strlen(rpc[i]);
		memcpy(pos, rpc[i], len + 1);   /* copy C-string including \0 */
		pos += len + 1;
	}
	
	*size = total;

	return result;
}
