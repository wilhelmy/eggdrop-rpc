#ifndef RPC_H
#define RPC_H

#include <stddef.h>

/* This function splits an RPC message into its array representation, provided
 * that it is complete. If the message was incomplete, the return value will be
 * NULL.
 *
 * The return value is allocated using malloc and must be freed after it is no
 * longer used. If malloc fails, errno is set to ENOMEM, so if you want to
 * check for out-of-memory conditions, you need to clear errno before calling
 * this function.
 *
 * In case it is non-NULL, the return value is a char pointer array containing
 * a NULL terminated list of pointers to offsets inside the data field where
 * the individual members are stored.
 *
 * This means that as long as the data is being processed, the memory pointed
 * to by the data pointer must not be disposed of.
 *
 * The rlen parameter, provided that it is non-NULL, will be filled with the
 * number of elements in the return value, so that rpc[rlen] will be the NULL
 * value at the end of the array.
 */
char const **rpc_split(char const *data, size_t len, size_t *rlen);

/* This function formats the rpc message array consisting of zero or more
 * C-strings and a NULL terminator into a byte array that's *not* a C-string
 * because it contains NUL bytes. Therefore, the length is returned via the
 * size pointer.
 *
 * It uses malloc to allocate a string that's long enough for all rpc entries,
 * and returns NULL in case the allocation failed.
 */
char *rpc_format(char const **rpc, size_t *size);

/* This function can be used to check if an RPC formatted message of length len
 * is a complete message by checking if the last 4 bytes are NULs.
 *
 * It returns 1 if the data is a complete message, 0 if it isn't.
 */
int rpc_complete(char const *data, size_t len);

#endif /* RPC_H */
