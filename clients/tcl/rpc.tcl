# Might work with earlier versions, but I haven't checked.
package require Tcl 8.5

namespace eval ::rpc {

# tests if a message has a trailer
proc complete {msg} {
	return [expr {[string range $msg end-3 end] eq "\0\0\0\0"}]
}

# Formats its argument list into an RPC message
proc format {arg} {
	if {[llength $arg] == 0} {
		error "can't encode empty list"
	}
	return "[join $arg \0]\0\0\0\0"
}

# like format, but puts it onto the socket, too
proc put {chan arg} {
	set data [rpc::format $arg]
	puts -nonewline $chan $data
	flush $chan
	return $data
}


# Splits a message using any previous buffered data. Automatically buffers
# things if they aren't complete. Returns "" if a buffer is not yet ready,
# which is an impossible message.
proc split {_buf msg} {
	upvar $_buf buf
	lappend buf $msg
	if {![complete $msg]} {
		return ""
	}
	set res [::split [string range [join $buf] 0 end-4] \0]
	set buf {}
	return $res
}

} ;# end namespace

package provide EggdropRPC 0.1
