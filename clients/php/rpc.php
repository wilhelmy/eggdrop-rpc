<?php

# This function is used to verify if a message is complete.
function rpc_complete($message)
{
	return substr($message, -4) === "\0\0\0\0";
}

# This function is used to split an RPC message into an array. It returns false 
# if the message is not complete, and the array otherwise.
function rpc_split($message)
{
	if (!rpc_complete($message))
		return false; 

	return explode("\0", substr($message, 0, -4));
}

# This function is used to format an RPC message from an array.
function rpc_format($array)
{
	return implode("\0", $array) . "\0\0\0\0";
}

?>
