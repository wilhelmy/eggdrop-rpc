Here's an overview over the TLS related options:

In order to use TLS at all, you will need to install the Tcl tls package and
set the use-tls setting to yes, but please read this file carefully first, and
follow the steps in the right order if you're new to using TLS. 

You will need a TLS private key and certificate.

To generate the private key, you can use one of the following commands:

	$ openssl genrsa -out rpc.key 2048
	or
	$ certtool -p --outfile rpc.key --sec-param normal

Afterwards, you need to decide whether you want a self-signed certificate or if
you want your certificate signed by a CA. How to get a certificate from a CA is
covered elsewhere. If you decide to generate a self-signed certificate, make
sure to check its fingerprint in your client application! Don't blindly trust
that your connection is not being tampered with.

To generate a self-signed certificate, use one of the following two commands:

	$ openssl req -new -x509 -key rpc.key -out rpc.cert -days 600
	or
	$ certtool -s --load-privkey rpc.key --outfile rpc.cert

Both of these commands are interactive, so you're going to have to hit enter a
few times. In this case, the questions they ask you won't matter, except
gnutls's certtool utility, which will ask you how many days you want your
certificate to be valid for, which is up to you (in openssl, that's controlled
with the "-days" switch, I picked 600 as a reasonable baseline.)

To find out about the fingerprint to include in your application, you can use
one of the following commands:

	$ openssl x509 -fingerprint < rpc.cert | sed -ne '/Fingerprint/ s/://gp'
	or
	$ certtool -i --infile rpc.cert | grep -A1 fingerprint

Now that you're done generating keys and certificates, you need to move them
somewhere on the machine where eggdrop is running and fix the paths in the
tls-cert and tls-key settings to point to the right files.

In tls-options, you can set further options and in tls-ciphers, you can change
which set of ciphers you want to use. If you have no idea what I'm talking
about, best leave them alone.

Client certificates are currently not supported by this project. If you would
like to have them, send me a message.
